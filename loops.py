import uuid


def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_number_from_20_to_30_multiplied_by_4():
    for number in range(20, 31):
        print(number * 4)


# Zadanie

def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


if __name__ == '__main__':
    print_random_uuids(7)

    print_number_from_20_to_30_multiplied_by_4()

    print_numbers_divisible_by_7(1, 30)
