import uuid


# Zadanie 1
# Napisz funkcję, która przyjmie argument w postaci listy liczb i zwróci ich średnią
# Napisz drugą funkcję, która przyjmie dwie liczby i zwróci ich średnią
# Z użyciem tych funkcji policz średnią temp. w ciągu dwóch miesięcy opisanych listami:
# january = [-4,1.0,-7,2]
# february = [-13, -9, -3, 3]
def get_average_of_list(list_with_numbers):
    return sum(list_with_numbers) / len(list_with_numbers)


def get_average_of_two_num(a, b):
    return (a + b) / 2


def run_exercise_1():
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]

    average_temperature = get_average_of_two_num(get_average_of_list(january), get_average_of_list(february))
    print('Average temperature in january and february:', average_temperature)


# Zadanie 2
# Utwórz funkcję generującą losowe dane do logowania.
# Argumentem jaki funkcja przyjmuje jest email użytkownika (np. "user@example.com")
# Funkcja powinna zwracać słownik postaci:
# {
# “email”: “user@example.com”,
# “password”: “536a7857-ca1c-4daf-93c3-0b92f5d724ee”
# }
# Gdzie hasło trzeba losowo wygenerować (pomocne: https://pynative.com/python-generate-random-string/ )

def get_user_credentials(email):
    get_user_credentials_dict = {'email': email, 'password:': str(uuid.uuid4())}
    return get_user_credentials_dict


def run_exercise_2():
    print(f'User credentials: {get_user_credentials("usera@example.com")}')
    print(f'User credentials: {get_user_credentials("userb@example.com")}')
    print(f'User credentials: {get_user_credentials("userc@example.com")}')
    print(f'User credentials: {get_user_credentials("usere@example.com")}')


# Zadanie 3
# Utwórz funkcję, która przyjmie słownik opisujący gracza:
# {
# “nick”: “maestro_54”,
# “type”: “warrior”,
# “exp_points”: 3000
# }
# i drukuje jego opis w postaci:
# “The player maestro_54 is of type warrior and has 3000 EXP”

def print_player_description(player_dictionary):
    print(
        f"The player {player_dictionary['nick']} is of type {player_dictionary['type']} and has {player_dictionary['exp_points']}."
    )


def run_exercise_3():
    player_1 = {
        'nick': 'maestro',
        'type': 'warrior',
        'exp_points': 3000
    }
    player_2 = {
        'nick': 'magician',
        'type': 'mage',
        'exp_points': 8888
    }
    player_3 = {
        'nick': 'omnibus_897',
        'type': 'mege',
        'exp_points': 99999
    }

    print_player_description(player_1)
    print_player_description(player_2)
    print_player_description(player_3)


if __name__ == '__main__':
    run_exercise_1()
    run_exercise_2()
    run_exercise_3()
