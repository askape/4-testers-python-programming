#Listy i słowniki są wieloelementowym typem zmiennych, czyli typem złożonym
#Listy mają indexy i elementy
#Słowniki mają klucze i wartości

#Listy
shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
print(shopping_list[0])   # wydrukuj 1 element z listy
print(shopping_list[-1])  # ostatni element
print(shopping_list[-2])  # przedostatni element

shopping_list.append('lemons')  # dodaj element do listy
print(shopping_list)

number_of_items_to_buy = len(shopping_list)   #długość listy

shopping_list.insert(0,"butter")  #wtawiamy na początku listy "butter"

first_three_shopping_items = shopping_list[0:3]    # w Pythonie zakres jest prawostronnie otwarty

print (first_three_shopping_items)
print(shopping_list)
number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)
shopping_list[-1] = "lemon"      #zamiana ostatniego elementu
print (shopping_list)


# Słowniki: struktura: nazwa_zmiennej = {"klucz":"wartość"}

animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}

dog_name = animal["name"]
print(dog_name)

dog_age = animal["age"]
print("Dog age:", dog_age)

animal["age"] = 10      #zmiana wartości dla danego klucza
print(animal)

animal["owner"] = "Staszek"     #dodanie nowego klucza i wartości
print(animal)




