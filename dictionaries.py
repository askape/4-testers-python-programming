actor = {
    "name": "Johnny Depp",
    "age": 60,
    "country": "United States",
    "alive": True
}

print(actor["age"])
actor["city"] = "Los Angeles"  # dopisanie nowego klucza i wartości do słownika
print(actor)
actor["age"] = 65  # zmiana wartości klucza "age" z 60 na 65
print(actor)
actor["hobbies"] = ["swimming", "dubbing", "box"]  # dopisanie nowego klucza, którego wartość jest typem listy
print(actor)
actor["job"] = "actor"
print(actor)
del actor["job"]  # usunięcie klucza razem z wartością
print(actor)

# Wydrukuj ostatnie hobby actora

print(actor["hobbies"][-1])  # trzeba zastosować podwójne indexowanie

# Dodaj kolejne hobby

actor["hobbies"].append("cycling")
print(actor)


def get_employee_info(email):
    return {
        'employee_email': email,
        'company': '4testers.pl'
    }


info_1 = get_employee_info('robert@4testers.pl')
info_2 = get_employee_info('magda@4testers.pl')
info_3 = get_employee_info('zofia@4testers.pl')

print(info_1)
print(info_2)
print(info_3)
