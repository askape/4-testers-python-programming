# Zadanie
# Stwórz listę zawierającą 3 adresy. Każdy adres jest postaci słownika zawierającego klucze:
# ● city
# ● street
# ● house_number
# ● post_code
# Wydrukuj:
# ● kod pocztowy ostatniego adresu
# ● Miasto z drugiego adresu
# Zmień ulicę pierwszego adresu na inną i wydrukuj całą listę

if __name__ == '__main__':

    addresses = [
    {
        "city": "Warsaw",
        "street": "Grójecka",
        "house_number": "2",
        "post_code": "12-123"
    },

    {
        "city": "Cracow",
        "street": "Mickiewicza",
        "house_number": "5",
        "post_code": "45-755"
    },

    {
        "city": "Gdańsk",
        "street": "Opatowska",
        "house_number": "45",
        "post_code": "78-896"
    }
]

    print(addresses[-1]["post_code"])
    print(addresses[1]["city"])
    addresses[0]["street"] = "Warszawska"

    print(addresses)
