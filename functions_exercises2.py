def print_a_car_brand_name():
    print("Honda")

def print_given_number_multiply_by_3(input_number):
    print(input_number * 3)

def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2

def calculate_area_of_a_triangle (bottom, height):
    return 0.5 * bottom * height


print_a_car_brand_name()
print_given_number_multiply_by_3(10)
area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)
area_a_little_triangle = calculate_area_of_a_triangle(5, 5)
print(area_a_little_triangle)

