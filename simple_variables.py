friend_name = 'Magda'
friend_age = 35
friend_number_of_pets = 2
driving_license = True
how_many_years_we_know = 4.5

print(friend_name,friend_age,friend_number_of_pets, driving_license, how_many_years_we_know,sep="\t")
print("Friend's name:", friend_name, "\nAge of friend:",friend_age, "\nFriend has", friend_number_of_pets,"pets", "\nDriging license:", driving_license, "\nFriendship time in years:", how_many_years_we_know  )
