# Zadanie 1
#Napisz funkcję, która zwraca kwadrat dowolnej liczby.
#Policz kwadraty liczb: 0, 16, 2.55
def print_square_of_given_number(input_number):
    return(input_number ** 2)

zero_squared = print_square_of_given_number(0)
sixteen_squarded = print_square_of_given_number(16)
floating_number_squeared = print_square_of_given_number(2.55)

print (zero_squared)
print (sixteen_squarded)
print (floating_number_squeared)


# Zadanie 2

#Napisz funkcję, która zwraca objętość prostopodłościanu o bokach: a,b,b
#Policz objetość prostopadłościanu o wymiarach 3x5x7

def print_volume_of_cuboid(a, b, c):
    return (a * b * c)


volume_of_cuboid = print_volume_of_cuboid(3,5,7)
print (volume_of_cuboid)


# Zadanie 3
# Napisz metodę, która konwertuje stoplnie Celsjusza na Fahrenheity.
#Ile Fahrenheitów to 20 st. C?
def convert_temperature_in_celsius_to_fahrenheit(temp_in_celsius):
    return 9 / 5 * temp_in_celsius + 32


fahrenheit = convert_temperature_in_celsius_to_fahrenheit(20)
print(fahrenheit)
