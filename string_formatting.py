my_name = "Asia"
favourite_movie = "Star Wars"
favourite_actor = "Clint Eastwood"

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

calculation_example = f"Sum of 2 and 4 is {2+4}."
print(calculation_example)

#Zadanie
#Napisz funkcję, która przyjmuje imię i miasto, a następnie drukuje napis powitalny:
#Witaj IMIĘ! Miło Cię widzieć w naszym mieście: MIASTO!"

def print_welcome_text(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")


print_welcome_text("Michał", "Toruń")
print_welcome_text("Beata", "Gdańsk")
print_welcome_text("adam", "kraków")

#Zadanie

#Napisz funkcję, która będzie generować adresy email w domenie firmy -> 4testers.pl
#Email ma formę "imie.nazwisko@4testers.pl
#Niech funkcja przyjmuje imię i nazwisko i zwraca utworzony email
#Wydrukuj emaile dla użytkowników: Janusz Nowak i Barbara Kowalska
def email_in_domain(name, last_name):
    return f"{name.lower()}.{last_name.lower()}@4testers.pl"


print(email_in_domain("Janusz", "Nowak"))
print(email_in_domain("Barbara", "Kowalska"))
print(email_in_domain("JOANNA", "KoWALCZYK"))
print(email_in_domain("joannA", "mRUCZEK"))


