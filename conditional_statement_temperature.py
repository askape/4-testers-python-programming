def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees.")
    if temperature_in_celsius > 25:
        print ("It's getting hot!")
    elif temperature_in_celsius > 0:
        print("It's quite OK.")
    else:
        print("It's getting cold:(")


def is_person_adult(age):
    # if age >= 18:
    #     return True
    # else:
    #     return False
    return True if age >= 18 else False     #zapis w prostej postaci, np. jeśli zwrócona wartość ma być tylko T or F

if __name__ == '__main__':
    temperature_in_celsius = 0
    print_temperature_description(temperature_in_celsius)
    age_kate = 17
    age_tom = 18
    age_marta = 21
    print("Kate", is_person_adult(age_kate))
    print("Tom", is_person_adult(age_tom))
    print("Marta", is_person_adult(age_marta))

