# Zadanie 1
# Stwórz funkcję przyjmującą listę temperatur (w st. Celsiusza) i
# drukuj każdą z temperatur w stopniach Celsiusza i Fahrenheitach.
# Użyj jej dla listy
# temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp * 9 / 5 + 32
        print(f'Celsius: {temp}, Fahrenheit: {temp_fahrenheit}')


def get_temperatures_higher_than_20_degrees(list_of_temps_in_celsius):
    filtered_temperatures = []
    for temp in list_of_temps_in_celsius:
        if temp > 20:
            filtered_temperatures.append(temp)
    return filtered_temperatures

# Zadanie 2
# Stwórz funkcję przyjmującą listę temperatur (w st. Celsiusza) i
# zwracającą listę w Fahrenheitach
# Użyj jej dla listy
# temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0

def convert_cesius_to_fahrenheit (list_of_temps_in_celsius):
    temps_fahrenheit = []
    for temp in list_of_temps_in_celsius:
        temps_fahrenheit.append(round(temp * 9 / 5 + 32))
    return temps_fahrenheit


if __name__ == '__main__':
#zadanie 1
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperatures_in_both_scales(temps_celsius)
    print(get_temperatures_higher_than_20_degrees(temps_celsius))
#zadanie 2
    print (convert_cesius_to_fahrenheit(temps_celsius))




