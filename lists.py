movies = ["Niezniszczalni", "IronMan", "Transformers", "Duna", "Szybcy i wściekli"]

# problem pojedynczego a podwójnego czudzysłowia
# restaurants = ['KFC', 'Burger King', 'McDonald\'s']     # użyj backslasha {\} żeby "wyescapować" apostrof, będzie to wydrukowane jako apostrof a nie traktowany jako koniec stringa


first_movie = movies[0]
print(f'First movie is \'{first_movie}\'.')
print(f'Last movie is \'{movies[-1]}\'.')   #ostatni film z listy

movies.append("Dune")   #dodawanie elementu do listy
print(f'The length of movies list is {len(movies)}.')

print(f'The length of word supercalifragilistic is {len("supercalifragilistic")}.')  #słowo to też lista znaków, przykłąd mieszania cudzysłowów, w tym przypadku nie da się tego zneutralizować \

movies.insert(0, "Star Wars")    #dodanie na pierwszym miejscu (index 0) filmu "Star Wars"
movies.remove("IronMan")           #usunięcie z listy filmy "IronMan"
print(movies)

movies[-1] = "Dune (1980)"   #podmienienie elementu o konkretnym indexie
print(movies)

# Zadanie
# Dla listy:
# emails = [‘a@example.com’, ‘b@example.com’]
# Wydrukuj:
# ● długość listy
# ● pierwszy element
# ● ostatni element
# Dodaj nowy email ‘cde@example.com

emails = ["a@example.com", "b@example.com"]
print (len(emails)) #długość listy
print (emails[0]) #pierwszy element z listy
print(emails[-1]) #ostatni element z listy

emails.append("cde@example.com") # dodanie elementu do listy
print(emails)


