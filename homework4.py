import random
import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

def generate_name(is_female):
    if is_female:
        return random.choice(female_fnames)
    else:
        return random.choice(male_fnames)

def is_adult(age):
    return True if age >= 18 else False

def generate_person_dictionary(is_female):
    generate_fname = generate_name(is_female)
    generate_surname = random.choice(surnames)
    generate_email = f'{generate_fname.lower()}.{generate_surname.lower()}@example.com'
    generate_age = random.randint(5, 45)
    generate_country = random.choice(countries)
    adult = is_adult(generate_age)
    year_of_birth = datetime.datetime.now().year - generate_age
    return {
        'firstname': generate_fname,
        'lastname': generate_surname,
        'email': generate_email,
        'age': generate_age,
        'country': generate_country,
        'is_adult': adult,
        'year_of_birth': year_of_birth
    }


def generate_list_of_people_with_5_female_names_and_5_male_names():
    generated_people = []
    for i in range(10):
        if i % 2 == 0:  # jeśli i jest podzielne przez dwa to do listy jest dopisywany słownik z imieniem żeńskim
            generated_people.append(generate_person_dictionary(True))
        else:
            generated_people.append(generate_person_dictionary(False))
    return generated_people


def print_description_for_every_person_from_the_list(people_dictionaries_list):
    for person in people_dictionaries_list:
        print(
            f'Hi! I\'m {person["firstname"]} {person["lastname"]}. I come from {person["country"]} and I was born in {person["year_of_birth"]}.')


if __name__ == '__main__':
    list_of_people = generate_list_of_people_with_5_female_names_and_5_male_names()
    print_description_for_every_person_from_the_list(list_of_people)
