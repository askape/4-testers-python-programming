def print_a_car_brand_name():
    print("Honda")

def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)

def calculate_area_of_a_circule(radius):
    return 3.1415 * radius ** 2

def upper_word(word):
    return word.upper()

def calculate_area_of_a_triangle(bottom, height):
    return 0.5 * bottom * height

print_a_car_brand_name()                                     #od razu printuje, bo taka jest funkcja
print_given_number_multiplied_by_3(10)                       #od razu printuje, bo taka jest funkcja
calculate_area_of_a_circule(10)                              # ta funkcja zwraca wartość, niczego nie printuje,
calculate_area_of_a_circule_radius_10 = calculate_area_of_a_circule(10)    #żeby zobaczyc tę warość musimy przypisać tę wartość do jakiejś zmiennej
print(calculate_area_of_a_circule_radius_10)                               #i wywołać 'printem'
area_a_little_triangle = calculate_area_of_a_triangle(5,5)
print (area_a_little_triangle)

# Checking what is returned from a function by default
result_of_a_print_function = print_a_car_brand_name()
print(result_of_a_print_function)


big_dog = upper_word("dog")
print(big_dog)
print(upper_word("tree"))




