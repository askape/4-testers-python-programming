first_name = "Joanna"
last_name = "Pich"
email = "joanna90pe.gmail.com"

print("Mam na imię", first_name, ".", "Moje nazwisko to", last_name, ".")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name +". Mój email to " + email + "."
print(my_bio)

#F-string
my_bio_using_f_string = f"Mam na imię {first_name}. \nMoje nazwisko to {last_name}. \nMój email to {email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}.")

### Algebra
circule_radius = 5
area_of_a_circle_with_radius_5 =3.14 * circule_radius **2
circumference_of_a_circle_with_radius_5 = 2 * 3.14 * circule_radius

print(f"Area of circle with radius {circule_radius}:", area_of_a_circle_with_radius_5)
print(f"Circumference of a circle with radius {circule_radius}:", circumference_of_a_circle_with_radius_5)

long_mathemacal_expression = 2 + 3 + 5 + 7 + 4 / 3 * (3 + 5 / 2) + 7**2
print(long_mathemacal_expression)