def print_word_length_description(name):
    if len(name) > 5:
        print(f'The name {name} is longer than 5 characters')
    else:
        print(f'The name {name} is 5 characters or shorter')

# Zadanie
# Zdefiniuj funkcję, która przyjmuje wartość temperatury (st.
# Celsiusza) i ciśnienia (hektopaskale), sprawdzającą czy są to
# warunki normalne.
# Jeśli temperatura jest równa 0 i ciśnienie jest równe 1013 -
# zwróć wartość True, w przeciwnym wypadku False
# Sprawdź wynik dla (0,1013); (1, 1013); (0, 1014); (1, 1014)
def check_if_normal_conditions(temp, pa):
    if temp == 0 and pa == 1013:
        return True
    else:
        return False
def is_it_4testers_day(day_of_week):
    if day_of_week == 'monday' or day_of_week == 'wednesday' or day_of_week == 'friday':
        return True
    else:
        return False


def is_it_japanese_car_brand(car_brand):
    if car_brand in ['Toyota', 'Suzuki', 'Mazda']:
        return True
    else:
        return False


# Zadanie
# Napisz funkcję, która przyjmuje wyniki na teście (punktacja 0-100)
# i zwraca ocenę:
# - 90% i więcej -> 5
# - 45% i więcej -> 4
# - 50% i więcej -> 3
# - poniżej 50% -> 2

def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >=75:
        return 4
    elif number_of_points >=50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    short_name = 'Asia'
    long_name = 'Adrian'

    print_word_length_description(long_name)
    print_word_length_description(short_name)

    print(check_if_normal_conditions(0, 1013))
    print(check_if_normal_conditions(1, 1014))
    print(check_if_normal_conditions(0, 1014))
    print(check_if_normal_conditions(1, 1014))

    print(is_it_4testers_day('monday'))
    print(is_it_4testers_day('thursday'))

    print(is_it_japanese_car_brand('BMW'))
    print(is_it_japanese_car_brand('Kia'))
    print(is_it_japanese_car_brand('Mazda'))


    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(90))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(74))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))

