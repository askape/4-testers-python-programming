name = 'John'
age = 25
height = 180.5

print(f'My name is {name} and I\'m {age} years old. \nMy height is {height}')